## 系统简介
首先感谢若依开源框架，这个框架完全免费，无需什么版权。小伙伴们可以完全放心的去学习使用！
地址：https://ruoyi.vip/
### 最新技术栈
使用最流行的技术SpringBoot、Shiro、Thymeleaf、Bootstrap。
#### 内置代码生成器
在线配置表信息生成对应的代码，一键生成模块，包含增删改查/排序/导出/权限控制等操作，编译即可使用
#### 我做的功能介绍
基于若依快速开发框架的中学选课系统，包括微信小程序；
##### 后端：若依快速开发框架 https://ruoyi.vip/
![](src/main/resources/static/img/img_8.png)
##### 前段小程序：微信小程序原生框架+ColorUI http://demo.color-ui.com/
![](src/main/resources/static/img/img_9.png)
本项目只是用来学习，请勿商业用途，我并不提供什么技术指导；
若您使用在正式项目中，请留下版权信息，版权信息为BahetCoder932696181。
若使用了本项目您的项目中，一旦出现问题，我一概不负责！
若您有更好的思路并且完善了本项目，请提供仓库地址跟大家分享，非常感谢；
开源不易请支持下，谢谢大家！
## 需要软件
我这里就不讲环境搭建了，不知道朋友们就先自学点基础吧！
1. 开发工具：IntelliJ IDEA 不知道激活的可以找我哦！推荐支持正版
2. 下载地址：https://www.jetbrains.com/idea/
3. Maven 5.4及以上
4. MySQL5.6及以上
## 功能介绍
除了若依框架内置的功能外，其他的功能是我加的，基本都增删改查，并不难的。
1. 学生信息 维护学生的基本信息
2. 课程信息 维护可选的课程信息
3. 基础配置 设置选课时间等等
4. 选课结果 选课后结果显示，可以导出或者可以修改掉的。
## 演示图片
### 后端演示图
![](src/main/resources/static/img/index.png)
![](src/main/resources/static/img/home.png)
![](src/main/resources/static/img/g1.png)
![](src/main/resources/static/img/g2.png)
![](src/main/resources/static/img/g3.png)
![](src/main/resources/static/img/g4.png)

### 小程序端演示图
![](src/main/resources/static/img/img.png) ![](src/main/resources/static/img/img_1.png)
![](src/main/resources/static/img/img_2.png) ![](src/main/resources/static/img/img_3.png)
![](src/main/resources/static/img/img_4.png) ![](src/main/resources/static/img/img_5.png)
![](src/main/resources/static/img/img_6.png) ![](src/main/resources/static/img/img_7.png)
## 项目视频演示地址
https://www.bilibili.com/video/BV1rq4y1F7AQ?spm_id_from=333.999.0.0

## 赞赏二维码
小伙伴 开源不易，请支持我，赞上下，感谢了！
![](src/main/resources/static/img/weixin.ed03e5b7.jpg)