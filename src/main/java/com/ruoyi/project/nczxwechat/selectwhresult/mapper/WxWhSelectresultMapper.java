package com.ruoyi.project.nczxwechat.selectwhresult.mapper;

import java.util.List;
import com.ruoyi.project.nczxwechat.selectwhresult.domain.WxWhSelectresult;

/**
 * 文化课程Mapper接口
 * 
 * @author 阿卜QQ932696181
 * @date 2022-01-19
 */
public interface WxWhSelectresultMapper 
{
    /**
     * 查询文化课程
     * 
     * @param id 文化课程主键
     * @return 文化课程
     */
    public WxWhSelectresult selectWxWhSelectresultById(Long id);

    /**
     * 查询文化课程列表
     * 
     * @param wxWhSelectresult 文化课程
     * @return 文化课程集合
     */
    public List<WxWhSelectresult> selectWxWhSelectresultList(WxWhSelectresult wxWhSelectresult);

    /**
     * 新增文化课程
     * 
     * @param wxWhSelectresult 文化课程
     * @return 结果
     */
    public int insertWxWhSelectresult(WxWhSelectresult wxWhSelectresult);

    /**
     * 修改文化课程
     * 
     * @param wxWhSelectresult 文化课程
     * @return 结果
     */
    public int updateWxWhSelectresult(WxWhSelectresult wxWhSelectresult);

    /**
     * 删除文化课程
     * 
     * @param id 文化课程主键
     * @return 结果
     */
    public int deleteWxWhSelectresultById(Long id);

    /**
     * 批量删除文化课程
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWxWhSelectresultByIds(String[] ids);

    /**
     * 通过学生唯一编号查询选课信息
     * @param stuno
     * @return
     */
    public WxWhSelectresult selectWxWhSelectresultByStuno(String stuno);
    /**
     * 通过学生唯一ID删除已选的课程信息
     * @param stuno
     * @return
     */
    public int deleteWxWhSelectresultByStuno(String stuno);
}
