package com.ruoyi.project.nczxwechat.whcours.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.nczxwechat.whcours.mapper.WxWhcoursMapper;
import com.ruoyi.project.nczxwechat.whcours.domain.WxWhcours;
import com.ruoyi.project.nczxwechat.whcours.service.IWxWhcoursService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 文化课程信息Service业务层处理
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-20
 */
@Service
public class WxWhcoursServiceImpl implements IWxWhcoursService 
{
    @Autowired
    private WxWhcoursMapper wxWhcoursMapper;

    /**
     * 查询文化课程信息
     * 
     * @param id 文化课程信息主键
     * @return 文化课程信息
     */
    @Override
    public WxWhcours selectWxWhcoursById(Long id)
    {
        return wxWhcoursMapper.selectWxWhcoursById(id);
    }

    /**
     * 查询文化课程信息列表
     * 
     * @param wxWhcours 文化课程信息
     * @return 文化课程信息
     */
    @Override
    public List<WxWhcours> selectWxWhcoursList(WxWhcours wxWhcours)
    {
        return wxWhcoursMapper.selectWxWhcoursList(wxWhcours);
    }

    /**
     * 新增文化课程信息
     * 
     * @param wxWhcours 文化课程信息
     * @return 结果
     */
    @Override
    public int insertWxWhcours(WxWhcours wxWhcours)
    {
        wxWhcours.setCreateTime(DateUtils.getNowDate());
        return wxWhcoursMapper.insertWxWhcours(wxWhcours);
    }

    /**
     * 修改文化课程信息
     * 
     * @param wxWhcours 文化课程信息
     * @return 结果
     */
    @Override
    public int updateWxWhcours(WxWhcours wxWhcours)
    {
        wxWhcours.setUpdateTime(DateUtils.getNowDate());
        return wxWhcoursMapper.updateWxWhcours(wxWhcours);
    }

    /**
     * 批量删除文化课程信息
     * 
     * @param ids 需要删除的文化课程信息主键
     * @return 结果
     */
    @Override
    public int deleteWxWhcoursByIds(String ids)
    {
        return wxWhcoursMapper.deleteWxWhcoursByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文化课程信息信息
     * 
     * @param id 文化课程信息主键
     * @return 结果
     */
    @Override
    public int deleteWxWhcoursById(Long id)
    {
        return wxWhcoursMapper.deleteWxWhcoursById(id);
    }
    /**
     * 根据学校ID查询学设置的课程列表
     * @param schoolid
     * @return
     */
    @Override
    public List<WxWhcours> selectWxWhcoursByShoolid(String schoolid) {
        return (List<WxWhcours>) wxWhcoursMapper.selectWxWhcoursByShoolid(schoolid);
    }
}
