package com.ruoyi.project.nczxwechat.stuinfo.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.nczxwechat.stuinfo.domain.WxStuinfo;
import com.ruoyi.project.nczxwechat.stuinfo.service.IWxStuinfoService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 学生信息Controller
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-16
 */
@Controller
@RequestMapping("/nczxwechat/stuinfo")
public class WxStuinfoController extends BaseController
{
    private String prefix = "nczxwechat/stuinfo";

    @Autowired
    private IWxStuinfoService wxStuinfoService;

    @RequiresPermissions("nczxwechat:stuinfo:view")
    @GetMapping()
    public String stuinfo()
    {
        return prefix + "/stuinfo";
    }

    /**
     * 查询学生信息列表
     */
    @RequiresPermissions("nczxwechat:stuinfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WxStuinfo wxStuinfo)
    {
        startPage();
        List<WxStuinfo> list = wxStuinfoService.selectWxStuinfoList(wxStuinfo);
        return getDataTable(list);
    }

    /**
     * 导出学生信息列表
     */
    @RequiresPermissions("nczxwechat:stuinfo:export")
    @Log(title = "学生信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WxStuinfo wxStuinfo)
    {
        List<WxStuinfo> list = wxStuinfoService.selectWxStuinfoList(wxStuinfo);
        ExcelUtil<WxStuinfo> util = new ExcelUtil<WxStuinfo>(WxStuinfo.class);
        return util.exportExcel(list, "学生信息数据");
    }

    /**
     * 新增学生信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存学生信息
     */
    @RequiresPermissions("nczxwechat:stuinfo:add")
    @Log(title = "学生信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WxStuinfo wxStuinfo)
    {
        return toAjax(wxStuinfoService.insertWxStuinfo(wxStuinfo));
    }

    /**
     * 修改学生信息
     */
    @RequiresPermissions("nczxwechat:stuinfo:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        WxStuinfo wxStuinfo = wxStuinfoService.selectWxStuinfoById(id);
        mmap.put("wxStuinfo", wxStuinfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存学生信息
     */
    @RequiresPermissions("nczxwechat:stuinfo:edit")
    @Log(title = "学生信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WxStuinfo wxStuinfo)
    {
        return toAjax(wxStuinfoService.updateWxStuinfo(wxStuinfo));
    }

    /**
     * 删除学生信息
     */
    @RequiresPermissions("nczxwechat:stuinfo:remove")
    @Log(title = "学生信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wxStuinfoService.deleteWxStuinfoByIds(ids));
    }
}
