package com.ruoyi.project.nczxwechat.stuinfo.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 学生信息对象 nczx_wx_stuinfo
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-16
 */
@Data
public class WxStuinfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 学生唯一ID */
    @Excel(name = "学生唯一ID")
    private String stuno;

    /** 学校代码 */
    @Excel(name = "学校代码")
    private String schooid;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String schoolname;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String stuname;

    /** 学生性别 */
    @Excel(name = "学生性别")
    private String stusex;

    /** 学生班级 */
    @Excel(name = "学生班级")
    private String stuclass;

    /** 用户openId */
    @Excel(name = "用户openId")
    private String openId;

    /**只是为了能接收参数，不需要存入数据库*/
    private String code;

    /** 用户微信昵称 */
    @Excel(name = "用户微信昵称")
    private String nickName;

    /** 用户微信头像地址 */
    @Excel(name = "用户微信头像地址")
    private String avatarUrl;

    /** 第一次登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "第一次登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    private Date firstLoginTime;

    /** 最后一次登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后一次登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    private Date lastLoginTime;

    /** 是否选课状态 */
    @Excel(name = "是否选课状态")
    private Long statues;
}
