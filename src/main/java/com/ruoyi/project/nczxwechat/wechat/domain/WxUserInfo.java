package com.ruoyi.project.nczxwechat.wechat.domain;

import lombok.Data;

/**
 * 接收前段传来的微信用户信息
 */
@Data
public class WxUserInfo {
    private String code;//临时code
    private String nickName;//微信昵称
    private  String avatarUrl;//微信头像
    private String stuno;//学生唯一ID
}
