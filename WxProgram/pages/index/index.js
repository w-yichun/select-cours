import util from '../../utils/util';

const app = getApp()

Page({
  data: {
    kxcourslist: [
      // {id:"1", title: '校本课', name: '人文', color: 'purple', icon: 'formfill' },
      // {id:"2", title: '研究性学习', name: '人文',color: 'mauve', icon: 'formfill' },
      // {id:"3", title: '社团', name: '自然科学', color: 'pink', icon: 'formfill' },
      // {id:"4", title: '文化课', name: '自然科学', color: 'cyan', icon: 'formfill' },
    ],
    swiperList:[
      {
        url:'/static/img/banner.jpg'
      },
    ],
    isLoad:true,
    token:wx.getStorageInfoSync("token"),
    endeTime:'2021-12-22 10:00:00',
    timedata:'00 天 00 小时 00 分钟 00 秒',
    istart:false,
    timer:0
  },

  onLoad() {
    // var that = this;
    // that.isFastTime();//检查登录
    },
    onShow(){
      var that = this;
      that.whcourselist();//文化信息加载
      // that.kxcourslist();//选课分类加载
      // that.getSetTime();//选课时时间获取
    },
   /**
   * 倒计时
   * 
   * @param endTime 结束日期+时间
   * **/
  countDown: function (endTime) {
    var that = this;
    that.setData({
      timer: setInterval(function () { //周期计时器，每隔1秒执行一次方法里的代码
        //得到一个从现在时间开始到活动结束的时间戳 
        var downTime = parseInt(new Date(endTime.replace(/-/g, "/")).getTime() - new Date().getTime());
        // 倒计时结束
        if (downTime <= 0) {
          that.setData({
            day: '00',
            hour: '00',
            minute: '00',
            second: '00',
            istart:true,
          })
          //结束周期计时器
          clearInterval(that.data.timer);
          return;
        }
        //计算距离活动还有多少天、时、分、秒
        var d = parseInt(downTime / 1000 / 3600 / 24);
        var h = parseInt(downTime / 1000 / 3600 % 24);
        var m = parseInt(downTime / 1000 / 60 % 60);
        var s = parseInt(downTime / 1000 % 60);
        //统一格式的显示
        d < 10 ? d = '0' + d : d;
        h < 10 ? h = '0' + h : h;
        m < 10 ? m = '0' + m : m;
        s < 10 ? s = '0' + s : s;
        //同步显示
        let timestr = d+" 天 "+h+" 小时 "+m+" 分钟 "+s+" 秒 "
        that.setData({
          timedata:timestr
        })
      }, 1000)
    })
  },
  /**
   * 进入选课页面
   * @param {*} e 
   */
  toselcat:function(e){
    wx.navigateTo({
      url: '../whcourse/whcourse',
    })
  },
    //详情
todetail:function(e){
      console.log(e);
       var selectstart = this.data.istart;
      var data = e.currentTarget.dataset
      if (this.data.token && selectstart) {
        wx.navigateTo({
          url: '../selectpage/index?title='+data.title+"&id="+data.id,
        })
      }else{
        console.log("未到选课时间")
        wx.showToast({
          title: '选课时间还未到，请耐心等待！',
          icon:'none',
          duration:2000
        })
      }
    },
//是否过期
isFastTime:function(){
    var userinfo = wx.getStorageSync('userInfo');
    util.get(`checkLoginTime?stuno=${userinfo.stuno}`,response=>{
        console.log(response)
        if(response){
          if (response.code==500) {
            console.log("登录信息过期！要清理缓存");
            wx.showModal({
              title: '提示',
              content: response.msg,
              showCancel:false,
              confirmColor:'#f00',
              success (res) {
                if (res.confirm) {
                  wx.clearStorageSync();
                    wx.navigateTo({
                      url: '/pages/home/index',
                    })
                } 
              }
            })
          }
        }else{
          wx.showToast({
            title: "访问失败，请稍后重试！",
            icon: 'error',
            duration: 2000
          })
        }
      })
},
//文化课程加载
whcourselist:function(){
  var userinfo = wx.getStorageSync('userInfo');
  util.get(`getWhCourseList?schoolid=${userinfo.schooid}`,response=>{
    console.log(response)
    if(response){
      if (response.code==0) {
        console.log("数据加载成功！");
        this.setData({
          kxcourslist:response.data,
        })
      }else{
        wx.showToast({
          title: '本校暂未设置选课！请耐心等待',
          icon:'none',
          duration:2000
        })
      }
      this.setData({
        isLoad:false
      })
    }
  })
},

//选课分类加载
kxcourslist:function(){
  util.get(`loadCoursMain`,response=>{
    console.log(response)
    if(response){
      if (response.code==0) {
        console.log("数据加载成功！");
        this.setData({
          kxcourslist:response.data,
        })
      }else{
        wx.showToast({
          title: '本校暂未设置选课！请耐心等待',
          icon:'none',
          duration:2000
        })
      }
      this.setData({
        isLoad:false
      })
    }
  })
},
//如果有选课列表就开始显示倒计时
getSetTime:function(){
  var userinfo = wx.getStorageSync('userInfo');
  util.get(`getSetTime?ognid=${userinfo.schooid}`,response=>{
    console.log(response)
    if(response){
      if (response.code==0) {
        console.log("数据加载成功！");
        this.countDown(response.data.starttime)
        this.setData({
          starttime:response.data.starttime,
          endtime:response.data.endtime
        })
      }else{
        wx.showToast({
          title: '暂未查到选课时间的配置信息',
          icon:'none',
          duration:2000
        })
      }
      this.setData({
        isLoad:false
      })
    }
  })
}
})
