import util from '../../utils/util';
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token:'',
    ischek:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    
  },

  onShow: function () {
    let that = this;
    const userinfo = wx.getStorageSync('userInfo');
    const token = wx.getStorageSync('token');
    console.log(userinfo)
    if (userinfo != null && userinfo != '') {
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  //获取学生信息
  confrimInfo:function(e){
    console.log(e)
    const that = this;
    const stuno = e.detail.value.stuno;
    console.log(stuno)
    if (e.detail.value.stuno.length!=0) {
          util.get(`getUserInfo?stuno=${stuno}`,response=>{
            console.log(response)
            if (response.code==0) {
              that.setData({
                //显示学校基本信息
                ischek:2,
                schoolname:response.data.schoolname,
                stuclass:response.data.stuclass,
                stuno:response.data.stuno,
                stuname:response.data.stuname,
                stusex:response.data.stusex
              })
            }else{
              wx.showToast({
                title: response.msg,
                icon: 'none',
                duration: 2000
              })
            }
          })
    }else{
      wx.showToast({
        title: '学生ID不能为空',
        icon: 'error',
        duration: 2000
      })
    }
  },
  //获取微信用户信息
  wxlogon:function(){
    const that = this;
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success:(res)=>{
        console.log("获取微信用户信息：",res)
          if(res){
            that.setData({
              ischek:1
            })
          }        
      },
      fail:function(e){
        console.log("拒绝登陆了",e)
        wx.showToast({
          title: '您授权登录失败了！',
          icon: 'error',
          duration: 2000
        })
      }
    })
  },
  //重置所有
  resetAll:function(){
    this.setData({
      ischek:0
    })
  },
  bindGetUserInfo(e) {
    let that = this
    //用户还未登录，申请用户授权
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log("userinfores----", res)
          let code = null
          wx.login({
            success: function (e) {
              code = e.code
              let params = {};
              params.code = code; //用户code  注:用户的code每次登录都是随机的，所以不需要进行存储
              params.avatarUrl = res.userInfo.avatarUrl; //用户头像
              params.nickName = res.userInfo.nickName; //用户微信名
            util.post('wxlogin',params,response=>{
              wx.showLoading({
                title: '登陆中...',
                mask:true
              })
              const data = response
              if (data.code === 200) {
                console.log("----登录成功----",data)
                //存储用户信息
                wx.setStorageSync('userInfo', data.userInfo);
                wx.setStorageSync('token', data.userInfo.openId);
                wx.hideLoading() //关闭提示框
                // wx.switchTab({
                //   url: '/pages/user/index/index', //跳转到指定页面
                // })
              }
            }
            )
            },
            fail(err) {
              console.log(err)
            }
          })
      },
      fail:function(e){
        console.log("拒绝登陆了",e)
      }
    })
  },
})