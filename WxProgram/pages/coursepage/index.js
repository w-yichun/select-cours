import util from '../../utils/util';

const app = getApp()

Page({
  data: {
    elements: [
      {id:"1", title: '语文', name: '人文', color: 'purple', icon: 'formfill' },
      {id:"1", title: '英语 ', name: '人文',color: 'mauve', icon: 'formfill' },
      {id:"1", title: '数学', name: '自然科学', color: 'pink', icon: 'formfill' },
      {id:"1", title: '物理', name: '自然科学', color: 'cyan', icon: 'formfill' },
      {id:"1", title: '化学', name: '自然科学', color: 'brown', icon: 'formfill' },
      {id:"1", title: '生物', name: '自然科学', color: 'red', icon: 'formfill' },
      {id:"1", title: '历史', name: '人文', color: 'orange', icon: 'formfill' },
      {id:"1", title: '地理', name: '自然科学', color: 'green', icon: 'formfill' },
      {id:"1", title: '政治', name: '人文', color: 'olive', icon: 'formfill' },
      {id:"1", title: '音乐', name: '艺体', color: 'grey', icon: 'formfill' },
      {id:"1", title: '美术', name: '艺体', color: 'cyan', icon: 'formfill' },
      {id:"1", title: '书法', name: '艺体', color: 'purple', icon: 'formfill' },
      {id:"1", title: '校园广播|主持人', name: '艺体', color: 'mauve', icon: 'formfill' },
      {id:"1", title: '体育', name: '艺体', color: 'pink', icon: 'formfill' },
      {id:"1", title: '心理', name: '艺体', color: 'brown', icon: 'formfill' },
      {id:"1", title: '通用技术',name: '创新',  color: 'red', icon: 'formfill' },
      {id:"1", title: '3D打印', name: '创新', color: 'orange', icon: 'formfill' },
      {id:"1", title: '机床', name: '创新', color: 'green', icon: 'formfill' },
      {id:"1", title: '机器人', name: '创新', color: 'olive', icon: 'formfill' },
      {id:"1", title: '无人机', name: '创新', color: 'purple', icon: 'formfill' },
      {id:"1", title: '航模', name: '创新', color: 'cyan', icon: 'formfill' }
    ],
    swiperList:[
      {
        url:'/static/img/banner.jpg'
      },
    ],
    token:wx.getStorageInfoSync("token"),
  },

  onLoad() {
    this.isFastTime();
    },
todetail:function(e){
      console.log(e);
      var data = e.currentTarget.dataset
      if (this.data.token) {
        wx.navigateTo({
          url: '../selectpage/index?title='+data.title+"&id="+data.id,
        })
      }else{
        console.log("未登录")
        wx.showModal({
          title: '登录提示',
          content: '请先登录后再操作！',
          success: function (sm) {
            if (sm.confirm) {
                wx.navigateTo({
                  url: '../login/index',
                })
              } else if (sm.cancel) {
                console.log('取消了')
              }
            }
          })
      }
    },
//是否过期
isFastTime:function(){
    var userinfo = wx.getStorageSync('userInfo');
    util.get(`checkLoginTime?stuno=${userinfo.stuno}`,response=>{
        console.log(response)
        // TIPS: 为0不一般是失败么
        if(response){
          if (response.code==500) {
            console.log("登录信息过期！要清理缓存");
            wx.showModal({
              title: '提示',
              content: response.msg,
              showCancel:false,
              confirmColor:'#f00',
              success (res) {
                if (res.confirm) {
                  wx.clearStorageSync();
                    wx.navigateTo({
                      url: '/pages/home/index',
                    })
                } 
              }
            })
          }
        }else{
          wx.showToast({
            title: "访问失败，请稍后重试！",
            icon: 'error',
            duration: 2000
          })
        }
      })
}
})
