import util from '../../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    minelist:[
      // {id:1,title:'红楼梦》整本书阅读',teacher:'赵洪英',desc:'通过对《红楼梦》进行研究，感受章回体小说独特的美。',time:'2021-9-01 22:22:22'},
      // {id:1,title:'红楼梦》整本书阅读',teacher:'赵洪英',desc:'通过对《红楼梦》进行研究，感受章回体小说独特的美。',time:'2021-9-01 22:22:22'}
    ],
    isLoad:true,
    isselect:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.myCoursList()
  },
  showModal(e) {
    console.log(e)
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  //课程信息加载
  myCoursList:function(){
    var userinfo = wx.getStorageSync('userInfo');
    util.get(`loadMyCoursList?stuno=${userinfo.stuno}`,response=>{
      console.log(response)
        if(response.code == 0){
          this.setData({
            isLoad:false,
            isselect:true,
            kcname:response.data.selectinfo,
            selecttime:response.data.createTime,
          })
        }else{
          this.setData({
            msg:response.msg
          })
          wx.showToast({
            title: response.msg,
            icon:'none',
            duration:5000
          })
        }
    })
  },
  //退选课程
  confirmModeal:function(){
    var userinfo = wx.getStorageSync('userInfo');
        let params = {
          stuno:userinfo.stuno
        }
    util.post('cancelCourse',params,response=>{
      const data = response
      if (data.code === 0) {
        console.log("----退选课成功！----",data)
        wx.hideLoading() //关闭提示框
        wx.switchTab({
          url: '/pages/index/index', //跳转到指定页面
        })
      }else{
        wx.showToast({
          title: data.msg,
          icon: 'none',
          duration: 2000
        })
      }
   })
    this.hideModal();
  }
})